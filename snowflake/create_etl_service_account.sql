use role securityadmin;
create role rl_gsp_etl comment = 'This role has all privileges on gsp, purpose is Dataload';
grant all on schema "GSP_DB"."GSP_SCHEMA" to role rl_gsp_etl;

create user gsp_etl password = '**************' default_role = rl_gsp_etl;
grant role rl_gsp_etl to user gsp_etl;
ALTER USER gsp_etl SET DEFAULT_WAREHOUSE = LOAD_WH;

grant usage on database "GSP_DB" to role rl_gsp_etl;
grant usage on schema "GSP_DB"."GSP_SCHEMA" to role rl_gsp_etl;
grant select on all tables in schema GSP_DB.GSP_SCHEMA to role rl_gsp_etl;
grant insert on all tables in schema GSP_DB.GSP_SCHEMA to role rl_gsp_etl;
grant select on all tables in schema GSP_DB.GSP_SCHEMA to role sysadmin;




use role rl_gsp_etl;
drop table "GSP_DB"."GSP_SCHEMA"."LOAD_STATUS";
CREATE TABLE "GSP_DB"."GSP_SCHEMA"."LOAD_STATUS" ("LOAD_DATE" STRING, "SCHEMA_NAME" STRING, "TABLE_NAME" STRING, "FILE_NAME" STRING);
grant insert on all tables in schema GSP_DB.GSP_SCHEMA to role rl_gsp_etl;
grant select on all tables in schema GSP_DB.GSP_SCHEMA to role sysadmin;