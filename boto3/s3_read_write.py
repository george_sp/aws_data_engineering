import boto3
from botocore.client import Config
from pathlib import Path
import pysftp

def list_files_src_s3(bucket, folder, file_prefix):
    list_of_files = []
    s3_client = boto3.resource('s3')
    src_bucket = s3_client.Bucket(bucket)
    for object_summary in src_bucket.objects.filter(Prefix=folder+file_prefix):
        print(object_summary.key)
        if object_summary.key.endswith(".dump"):
            list_of_files.append(object_summary.key.split(folder)[-1])
    return set(list_of_files)


def write_files_dest_s3(credentials,folder, file):
        config = Config(connect_timeout=20,read_timeout=60, retries={'max_attempts': 0})
        s3 = boto3.client('s3', config=config)  
        #s3 = boto3.resource('s3', config=config)
        print(credentials['s3_bucket'])
        #bucket = s3.Bucket(name=credentials['s3_bucket'])
        #bucket.load()
        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None  
        with pysftp.Connection(host = credentials['sftp_host_name'],
                           username = credentials['sftp_user_name'],
                           password = credentials['sftp_password'],
                             cnopts = cnopts) as sftp:
            sftp.cwd(folder)
            with sftp.open(file, 'rb') as file_obj:
                file_obj.prefetch()
                print(file_obj)
                print(f"uploading  {file} to s3...")
                            
                response = s3.put_object(
                    Bucket=credentials['s3_bucket'], Key="postup/"+file, Body=file_obj.read()
                )
                status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
                if status == 200:
                    print(f"Successful S3 put_object response. Status - {status}")
                else:
                    print(f"Unsuccessful S3 put_object response. Status - {status}")
                
                print(f"uploading Completed for {file}")