# AWS
- ## [Airflow (MWAA)](https://gitlab.com/george_sp/aws_data_engineering/-/tree/main/airflow_mwaa)
     - ### [DAGS](https://gitlab.com/george_sp/aws_data_engineering/-/tree/main/airflow_mwaa/modules/dags)
- ## [Glue ETL](https://gitlab.com/george_sp/aws_data_engineering/-/tree/main/glue_etl)
- ## [Boto3](https://gitlab.com/george_sp/aws_data_engineering/-/tree/main/boto3)


# Python
- ## Database Connectors
- ## Pandas
- ## Utilities


# SPARK
- ## DataFrame  from  - csv|json|parquet|avro|jdbc|s3

# SNOWFLAKE
- ## [CREATE ETL SERVICE ACCOUNT](https://gitlab.com/george_sp/aws_data_engineering/-/blob/main/snowflake/create_etl_service_account.sql)

