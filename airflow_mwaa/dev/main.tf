terraform {
  required_version = ">= 1.0.8"
  backend "s3" {
    bucket         = "datanation-terraform-tfstate"
    key            = "datanation-terraform-tfstate-airflow-dev.tfstate"
    region         = "us-east-2"
    encrypt        = true
    dynamodb_table = "datanation-terraform-tflock-airflow-dev"

  }
}

provider "aws" {
  region = var.AWS_REGION
}

# Create Resource for Development Environment

module "mwaa" {
    source      = "../modules/mwaa"

    #ENVIRONMENT = var.ENV
    #AWS_REGION  = var.AWS_REGION
    #APP_NAME    = var.APP_NAME
    #TAGS        = var.TAGS
    region               = var.AWS_REGION
    prefix               = format("%s-%s", var.APP_NAME, var.ENV)
    vpc_cidr             = var.vpc_cidr
    public_subnet_cidrs  = var.public_subnet_cidrs
    private_subnet_cidrs = var.private_subnet_cidrs
    mwaa_max_workers                = var.mwaa_max_workers
    client_vpn_cidr_block           = var.client_vpn_cidr_block
    vpn_acm_validity_period_in_days = var.vpn_acm_validity_period_in_days
    s3_buckets_rw                   = var.s3_buckets_rw
}
