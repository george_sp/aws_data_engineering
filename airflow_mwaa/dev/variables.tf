# Input variables
variable "ENV" {
    type    = string
    default = "dev"
}

variable "AWS_REGION" {
  type        = string
  description = "AWS region where resources will be deployed."
  default = "us-east-2"
}

variable "APP_NAME" {
  type        = string
  description = "A prefix to use when naming resources. Only alpha and hypens"
  default = "datanation-airflow"
}

variable "vpc_cidr" {
  type        = string
  description = "VPC CIDR block."
  default = "10.44.22.0/24"
}

variable "public_subnet_cidrs" {
  type        = list(string)
  description = "Public subnets' CIDR blocks."
  default = [
  "10.44.22.0/28",
  "10.44.22.16/28",
]
}

variable "private_subnet_cidrs" {
  type        = list(string)
  description = "Private subnets' CIDR blocks."
  default = [
  "10.44.22.32/27",
  "10.44.22.64/27",
]
}

variable "mwaa_max_workers" {
  type        = number
  description = "Maximum number of MWAA workers."
  default     = 2
}

variable "client_vpn_cidr_block" {
  type        = string
  description = "Client CIDR block for MWAA client VPN."
  default     = "10.0.0.0/22"
}

variable "vpn_acm_validity_period_in_days" {
  type        = number
  description = "Amount of days after which TLS certificates used for MWAA client VPN should expire."
  default     = 1095 # 3 years
}

variable "TAGS" {
  description  = " map of tags required for entercon AWS resources"
  type = object ({
      TerraformRepo  = string
      Terraform      = string
      Application    = string
      Team           = string
      BusinessUnit   = string
      LastModifiedBy = string
  })
    default = {
      TerraformRepo  = "NA"
      Terraform      = "true"
      Application    = "datanation-airflow"
      Team           = "Data_Engineering"
      BusinessUnit   = "Data"
      LastModifiedBy = "george.pulinattu@gsp.com"
  }
}


variable "s3_buckets_rw" {
  type        = list(string)
  description = "Buckets that need read write access."
  default = [
      "arn:aws:s3:::bucket1",
      "arn:aws:s3:::bucket1/gimbal/*",
      "arn:aws:s3:::bucket1/bds/*",
      "arn:aws:s3:::bucket1/postup/*",
      "arn:aws:s3:::bucket1/fusionauth/*",
      "arn:aws:s3:::bucket2",
      "arn:aws:s3:::bucket2/*",
      "arn:aws:s3:::bucket3",
      "arn:aws:s3:::bucket3/*",
      "arn:aws:s3:::bucket4",
      "arn:aws:s3:::bucket4/rate-optics/*",
]
}
