# Deploy Using Terraform

- Step 1: ./dev/mail.tf > update the backends
- Step 2: ./dev/variables.tf > updated the required variables

````
aws-vault exec aws-use-profile --duration=12h

DEVELOPMENT:

docker-compose -f docker-compose.yml run -w /infra/dev --rm terraform validate
docker-compose -f docker-compose.yml run -w /infra/dev --rm terraform version

docker-compose -f docker-compose.yml run -w /infra/dev --rm terraform init


docker-compose -f docker-compose.yml run -w /infra/dev --rm terraform plan
docker-compose -f docker-compose.yml run -w /infra/dev --rm terraform apply


PRODUCTION:
docker-compose -f docker-compose.yml run -w /infra/prd --rm terraform validate
docker-compose -f docker-compose.yml run -w /infra/prd --rm terraform version

docker-compose -f docker-compose.yml run -w /infra/prd --rm terraform init


docker-compose -f docker-compose.yml run -w /infra/prd --rm terraform plan
docker-compose -f docker-compose.yml run -w /infra/prd --rm terraform apply
````

# Sample Secretsmanager Credentials
~~~~
secrets_name : airflow/connections/postup_sftp
{
  "sftp_host_name": "transfer.gsp.com",
  "sftp_user_name": "gsp_ftp_user",
  "sftp_password": "********",
  "file_list_prefix": "folder-1,folder-2,folder-3",
  "s3_bucket": "gsp-staging-bucket",
  "snow_user": "gsp_etl",
  "snow_password": "********************",
  "snow_account": "datanation.us-east-1",
  "snow_warehouse": "LOAD_WH",
  "snow_database": "GSP_DB",
  "snow_schema": "GSP_SCHEMA"
}
~~~~