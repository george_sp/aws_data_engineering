# S3

resource "aws_s3_bucket" "s3_bucket" {
  bucket = var.prefix

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = true
  }

  tags = merge(local.tags, {
    Name = var.prefix
  })
}

resource "aws_s3_bucket_public_access_block" "s3_bucket_public_access_block" {
  bucket                  = aws_s3_bucket.s3_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_object" "dags" {
  for_each = fileset("../modules/dags/", "*.py")
  bucket   = aws_s3_bucket.s3_bucket.id
  key      = "dags/${each.value}"
  source   = "../modules/dags/${each.value}"
  etag     = filemd5("../modules/dags/${each.value}")
}

resource "aws_s3_bucket_object" "plugins" {
  for_each = fileset("../modules/plugins/", "*")
  bucket   = aws_s3_bucket.s3_bucket.id
  key      = "plugins/${each.value}"
  source   = "../modules/plugins/${each.value}"
  etag     = filemd5("../modules/plugins/${each.value}")
}

resource "aws_s3_bucket_object" "requirements" {
  for_each = fileset("../modules/requirements/", "*.txt")
  bucket   = aws_s3_bucket.s3_bucket.id
  key      = "requirements/${each.value}"
  source   = "../modules/requirements/${each.value}"
  etag     = filemd5("../modules/requirements/${each.value}")
}
