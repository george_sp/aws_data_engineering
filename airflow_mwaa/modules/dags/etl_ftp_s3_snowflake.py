import boto3
from botocore.client import Config
from airflow import DAG, settings, secrets
from airflow.utils.dates import days_ago
from airflow.providers.amazon.aws.hooks.base_aws import AwsBaseHook
from airflow.contrib.operators.sftp_to_s3_operator import SFTPToS3Operator
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.S3_hook import S3Hook
import pysftp
import re
import os
import io
import json
import sys
import csv
from datetime import datetime,timedelta
import snowflake.connector
from snowflake.connector.pandas_tools import write_pandas
import logging
import pandas as pd
from contextlib import redirect_stderr
import pprint
#from smart_open import open

sm_secretId_name = 'airflow/connections/gspapp_sftp'

default_args = {
    'owner': 'airflow',
    'start_date': days_ago(1),
    'depends_on_past': False
}


### Gets the secret gspapp from Secrets Manager
def read_from_aws_sm_fn():
    ### set up Secrets Manager
    hook = AwsBaseHook(client_type='secretsmanager')
    client = hook.get_client_type('secretsmanager')
    response = client.get_secret_value(SecretId=sm_secretId_name)
    myConnSecretString = response["SecretString"]
    ConnSecretString = json.loads(myConnSecretString)
    #print(ConnSecretString['s3_source_bucket'])
    return ConnSecretString

def list_files_src_sftp(credentials, folder, file_prefix):
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None  
    server = pysftp.Connection(host = credentials['sftp_host_name'],
                           username = credentials['sftp_user_name'],
                           password = credentials['sftp_password'],
                             cnopts = cnopts
                             )
    server.cwd(folder)
    filelist = server.listdir()
    files_in_folder = []
    for filename in filelist:
        #print(filename)
        if filename.startswith(file_prefix):
            #print(filename)
            files_in_folder.append(filename)
    return set(files_in_folder)

def list_files_dest_s3(bucket, folder, file_prefix):
    list_of_files = []
    s3_client = boto3.resource('s3')
    dest_bucket = s3_client.Bucket(bucket)
    for object_summary in dest_bucket.objects.filter(Prefix=folder+file_prefix):
        print(object_summary.key)
        if object_summary.key.endswith(".csv"):
            list_of_files.append(object_summary.key.split(folder)[-1])
    return set(list_of_files)


def write_files_dest_s3(credentials,folder, file):
        config = Config(connect_timeout=20,read_timeout=60, retries={'max_attempts': 0})
        s3 = boto3.client('s3', config=config)  
        #s3 = boto3.resource('s3', config=config)
        print(credentials['s3_bucket'])
        #bucket = s3.Bucket(name=credentials['s3_bucket'])
        #bucket.load()
        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None  
        with pysftp.Connection(host = credentials['sftp_host_name'],
                           username = credentials['sftp_user_name'],
                           password = credentials['sftp_password'],
                             cnopts = cnopts) as sftp:
            sftp.cwd(folder)
            with sftp.open(file, 'rb') as file_obj:
                file_obj.prefetch()
                print(file_obj)
                print(f"uploading  {file} to s3...")
                            
                response = s3.put_object(
                    Bucket=credentials['s3_bucket'], Key="gspapp/"+file, Body=file_obj.read()
                )
                status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
                if status == 200:
                    print(f"Successful S3 put_object response. Status - {status}")
                else:
                    print(f"Unsuccessful S3 put_object response. Status - {status}")
                
                print(f"uploading Completed for {file}")

def gspapp_ingest_to_s3 (**kwargs):
    gspapp_vars = read_from_aws_sm_fn()
    
    folders_list = gspapp_vars['file_list_prefix'].split(",")
    for prefix in folders_list:
        print("Processing :" +prefix)
        set_of_files_src = list_files_src_sftp(credentials=gspapp_vars, folder ="/", file_prefix = prefix)
        set_of_files_dst = list_files_dest_s3(bucket=gspapp_vars['s3_bucket'], folder ="gspapp/", file_prefix = prefix)
        incremental_file_list = list(set_of_files_src.difference(set_of_files_dst))
        print(set_of_files_src)
        print(set_of_files_dst)
        incremental_file_list.sort(reverse=True)
        for file in incremental_file_list[:3]:
            print("Writing File : "+file)
            write_files_dest_s3(credentials=gspapp_vars, folder ="/", file=file)

############## DAG To Load Snowflake ###############
def list_of_loaded_snowflake(con, table_name):
    curs=con.cursor()
    return_list=[]
    pre_load_check = curs.execute("select FILE_NAME from LOAD_STATUS where table_name='"+table_name+"'").fetchall()
    #print(pre_load_check)
    for items in pre_load_check:
        #print(items[0])
        return_list.append(items[0].split('/')[-1])
    return set(return_list)

def s3_to_snow_load (bucket_name,file_name,connection, gspapp_vars, table_name):
    DATE_LOAD_TAG = file_name.split('/')[-1].split('.')[0].replace("-", "")
    s3_client = boto3.client( "s3")
    response = s3_client.get_object(Bucket=bucket_name, Key=file_name)
    status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
    if status == 200:
        print(f"Successful S3 get_object response. Status - {status}")
        omit = []
        omited_vals ={}
        body = response['Body']
        csv_string = body.read().decode('utf-8')
        
        with io.StringIO(csv_string) as csv_file:
            reader = csv.reader(csv_file, delimiter=',')
            header = next(reader)
            for i, row in enumerate(reader, 1):
                if (len(row) != len(header)):
                    omit.append(i)
                    omited_vals[i]=row
        print(omit)
        #pprint.pprint(omited_vals)
        if len(omited_vals) != 0:
            column_names = ["ROW_ID", "ROW_VALUES", "FILENAME"]
            df_garbage = pd.DataFrame(columns = column_names)
            schema_sf_lst_garbage=[]
            for key, value in omited_vals.items():
                # append rows to an empty DataFrame
                df_garbage = df_garbage.append({'ROW_ID' : key, 'ROW_VALUES' : value, 'FILENAME' : file_name}, ignore_index = True)
            for y in column_names:
                schema_sf_lst_garbage.append(y+" string" )
            schema_sf_lst_garbage = ', '.join(schema_sf_lst_garbage)
            sql_create_table_gar = "CREATE TABLE IF NOT EXISTS "+gspapp_vars['snow_database']+"."+gspapp_vars['snow_schema']+"."+table_name+"_GARBAGE ("+schema_sf_lst_garbage+")"
            print(sql_create_table_gar)
            cur = connection.cursor()
            cur.execute(sql_create_table_gar)
            cur.close()
            success_0, nchunks_0, nrows_0, _ = write_pandas(conn=connection, df=df_garbage, table_name=table_name+"_GARBAGE" )
            if success_0:
                logging.info("Status of "+table_name+"_GARBAGE load:" + str(success_0))
                logging.info("Chunks of "+table_name+"_GARBAGE load:" + str(nchunks_0))
                logging.info("Rows loaded to "+table_name+"_GARBAGE load:" + str(nrows_0))

        #df_csv = pd.read_csv(response.get("Body"), dtype = str, skiprows=omit)
        #print(df_csv.head(3))
        #err_log = io.StringIO()
        #with redirect_stderr(err_log):
            #file_df = pd.read_csv(response.get("Body"), dtype = str, warn_bad_lines=True, error_bad_lines=False)
        #print("Printing Error")
        #contents = err_log.getvalue().encode('utf-8')
        #print(contents)
        #print("Printing Dataframe")
        file_df = pd.read_csv(io.StringIO(csv_string) , dtype = str, skiprows=omit)
        print(file_df.head(3))
        col_dict={}
        schema_sf_lst=[]
        for y in file_df.columns:
            #print(y)
            if y not in col_dict.keys():
                refined_col = y.replace(" ", "_").replace("&", "").replace("%", "").replace("(", "").replace(")", "").replace("+", "plus").replace("-", "_").upper()
                col_dict[y]= refined_col
                schema_sf_lst.append(refined_col+" string" )
            else:
                print("Duplicate Columns :" + y)

        #print(col_dict)
        schema_sf_lst.append("DATE_LOAD_TAG VARCHAR(50)" )
        schema_sf_lst = ', '.join(schema_sf_lst)
        #print(schema_sf_lst)
        file_df = file_df.rename(columns = col_dict)
        file_df['DATE_LOAD_TAG'] = DATE_LOAD_TAG
        sql_create_table = "CREATE TABLE IF NOT EXISTS "+gspapp_vars['snow_database']+"."+gspapp_vars['snow_schema']+"."+table_name+" ("+schema_sf_lst+")"
        print(sql_create_table)
        cur = connection.cursor()
        cur.execute(sql_create_table)
        cur.close()
        success, nchunks, nrows, _ = write_pandas(conn=connection, df=file_df, table_name=table_name)
        if success:
            connection.cursor().execute("INSERT into LOAD_STATUS (LOAD_DATE, SCHEMA_NAME, TABLE_NAME, FILE_NAME)VALUES('"+DATE_LOAD_TAG+"', 'gspapp', '"+table_name+"', '"+file_name+"' )")
        logging.info("Status of "+table_name+" load:" + str(success))
        logging.info("Chunks of "+table_name+" load:" + str(nchunks))
        logging.info("Rows loaded to "+table_name+" load:" + str(nrows))
    else:
        print(f"Unsuccessful S3 get_object response. Status - {status}")
def gspapp_ingest_to_snowflake (**kwargs):
    gspapp_vars = read_from_aws_sm_fn()
    #print(gimbal_vars)
    con = snowflake.connector.connect(
                                        user      = gspapp_vars['snow_user'],
                                        password  = gspapp_vars['snow_password'],
                                        account   = gspapp_vars['snow_account'],
                                        warehouse = gspapp_vars['snow_warehouse'],
                                        database  = gspapp_vars['snow_database'],
                                        schema    = gspapp_vars['snow_schema'],
                                        session_parameters={
                                                           'QUERY_TAG': 'gspapp_Load',
                                                           }
                                        )
    folders_list = gspapp_vars['file_list_prefix'].split(",")
    #folders_list=['MailingReportMonthly_']
    for src_folder in folders_list:
        print("Processing folder :" + src_folder)
        already_loaded_to_snow = list_of_loaded_snowflake(con=con, table_name=src_folder.upper()[:-1])
        list_of_files_src = list_files_dest_s3(bucket=gspapp_vars['s3_bucket'], folder ="gspapp/", file_prefix=src_folder)
        #print (already_loaded_to_snow)
        set_list_of_files_dst = already_loaded_to_snow
        set_list_of_files_src = set(list_of_files_src)
        print(set_list_of_files_dst)
        print(set_list_of_files_src)
        incremental_file_list = list(set_list_of_files_src.difference(set_list_of_files_dst))
        incremental_file_list.sort()
        print(incremental_file_list)
        for file in incremental_file_list[:5]:
            print("Loading :" +file )
            s3_to_snow_load (bucket_name=gspapp_vars['s3_bucket'],file_name="gspapp/"+file,connection=con, gspapp_vars=gspapp_vars, table_name = src_folder.upper()[:-1])

with DAG(
        dag_id=os.path.basename(__file__).replace(".py", ""),
        default_args=default_args,
        dagrun_timeout=timedelta(hours=2),
        schedule_interval="0 10 * * *",
        start_date=days_ago(1),
) as dag:
    write_all_to_aws_s3 = PythonOperator(
        task_id="gspapp_ingest_to_s3",
        python_callable=gspapp_ingest_to_s3,
        provide_context=True
    )
    write_all_to_snow = PythonOperator(
        task_id="gspapp_ingest_to_snowflake",
        python_callable=gspapp_ingest_to_snowflake,
        provide_context=True
    )

write_all_to_aws_s3 >> write_all_to_snow
