import boto3
from botocore.client import Config
from airflow import DAG, settings, secrets
from airflow.utils.dates import days_ago
from airflow.providers.amazon.aws.hooks.base_aws import AwsBaseHook
from airflow.contrib.operators.sftp_to_s3_operator import SFTPToS3Operator
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.S3_hook import S3Hook
from airflow import AirflowException
import pysftp
import re
import os
import json
import sys
import csv
import gc
from datetime import datetime,timedelta
import snowflake.connector
from snowflake.connector.pandas_tools import write_pandas
import logging
import pandas as pd
from contextlib import redirect_stderr
from pprint import pprint as pprint
import gzip
from io import BytesIO
from threading import Thread
try:
    import queue as Queue
except ImportError:
    import Queue
import slack



sm_secretId_name = 'airflow/connections/gspapp_gsptablelogs'

default_args = {
    'owner': 'airflow',
    'start_date': days_ago(1),
    'depends_on_past': False
}

class BucketConn(object):
    def __init__(self,key_id,access_key):
       self.aws_access_key_id = key_id 
       self.aws_secret_access_key = access_key

    def s3_resource(self):
        self.session = boto3.Session(
                                aws_access_key_id=self.aws_access_key_id,
                                aws_secret_access_key=self.aws_secret_access_key,
                               )
        return self.session.resource('s3')
         
    def s3_client(self):
        self.session = boto3.Session(
                        aws_access_key_id=self.aws_access_key_id,
                        aws_secret_access_key=self.aws_secret_access_key,
                       )
        return self.session.client('s3')
    
class DataTypeMapper(object):
    def __init__(self,current_map_lst):
        self.current_map_lst = current_map_lst
        self.lookup_colname   = {"COLUMN_A":"float","COLUMN_A":"int","COLUMN_A":"string","COLUMN_A":"bool","COLUMN_A":"string(10)","COLUMN_A":"string","COLUMN_A":"int","COLUMN_A":"int","COLUMN_A":"float"}
        self.lookup_pandas    = {"string":"string", "int":"int64", "float":"float64", "bool":"bool", "JSON":"string","url":"string","date":"datetime64", "time":"datetime64[ns]","epoch":"int64"}
        self.lookup_spark     = {"string":"StringType()", "int":"IntegerType()", "float":"FloatType()", "bool":"BooleanType()", "JSON":"StringType()","url":"StringType()","date":"DateType()", "time":"TimestampType()","epoch":"LongType()"}
        self.lookup_snowflake = {"string":"VARCHAR", "int":"INTEGER", "float":"FLOAT", "bool":"BOOLEAN", "JSON":"VARCHAR","url":"VARCHAR","date":"DATE", "time":"TIMESTAMP","epoch":"INTEGER"}

    def pandas_datatype(self):
        self.data_type_mapped = {}
        for col_key in self.current_map_lst:
            if col_key in self.lookup_colname:
                try:
                    precision = int(re.search(r'\d+', self.lookup_colname[col_key]).group())
                except:
                    precision=''
                if precision:
                    d_type = self.lookup_colname[col_key].split("(")
                    self.data_type_mapped[col_key]=self.lookup_pandas[d_type[0]]
                else:
                    self.data_type_mapped[col_key]=self.lookup_pandas[self.lookup_colname[col_key]]
            else:
                self.data_type_mapped[col_key]="string"
        return self.data_type_mapped

    
    def spark_datatype(self):
        self.data_type_mapped = {}
        for col_key in self.current_map_lst:
            if col_key in self.lookup_colname:
                try:
                    precision = int(re.search(r'\d+', self.lookup_colname[col_key]).group())
                except:
                    precision=''
                if precision:
                    d_type = self.lookup_colname[col_key].split("(")
                    self.data_type_mapped[col_key]=self.lookup_spark[d_type[0]]+"("+str(precision)+")"
                else:
                    self.data_type_mapped[col_key]=self.lookup_spark[self.lookup_colname[col_key]]
            else:
                self.data_type_mapped[col_key]="StringType()"
        return self.data_type_mapped

    def snow_datatype(self):
        self.data_type_mapped = {}
        for col_key in self.current_map_lst:
            if col_key in self.lookup_colname:
                try:
                    precision = int(re.search(r'\d+', self.lookup_colname[col_key]).group())
                except:
                    precision=''
                if precision:
                    d_type = self.lookup_colname[col_key].split("(")
                    self.data_type_mapped[col_key]=self.lookup_snowflake[d_type[0]]+"("+str(precision)+")"
                else:
                    self.data_type_mapped[col_key]=self.lookup_snowflake[self.lookup_colname[col_key]]
            else:
                self.data_type_mapped[col_key]="VARCHAR"
        return self.data_type_mapped



### Gets the secret from Secrets Manager
def read_from_aws_sm_fn(sm_secretId_name):
    ### set up Secrets Manager
    hook = AwsBaseHook(client_type='secretsmanager')
    client = hook.get_client_type('secretsmanager')
    response = client.get_secret_value(SecretId=sm_secretId_name)
    myConnSecretString = response["SecretString"]
    ConnSecretString = json.loads(myConnSecretString)
    return ConnSecretString


def chunks_gen(l, n):
    """Yield n number of striped chunks from l."""
    for i in range(0, n):
        yield l[i::n]


def batch_files(lst,batch_size):
    '''
    If you give a list object lst and no: of batch_size
    This function retun a list of list
    There will be 'batch_size' lists in the list that it returns.
    eg: L1 is a list of numbers from 1-100 and batch_size is 10 this will return a list of 10 lists and each list has 10 numbers.
    '''
    list_of_list=[]
    for  items in chunks_gen(lst,batch_size):
        if items:
            list_of_list.append(items)
    return list_of_list

def cameltosnake(string_var):
    '''
    abCdef returns ABC_DEF
    GhiJklmN returns GHIJ_KLMN
    '''
    new_string =""
   
    for i in range(len(string_var)):
        if i > 0 :
            if string_var[i-1].islower() and string_var[i].isupper():
                new_string = new_string+"_"+string_var[i]
            else:
               new_string = new_string+string_var[i] 
        else:
            new_string = new_string+string_var[i]

    return new_string

def list_files_src_s3(var, file_prefix, file_type):
    '''
    Will return a list of objects in a s3 bucket.
    eg: S3://file_prefix/object.file_type returns [object1.file_type,object2.file_type,object3.file_type]
    [object1.gz,object2.gz,object3.gz] or [object1.csv,object2.csv,object3.csv]
    '''
    s3_con = BucketConn(key_id=var['aws_access_key_id'],access_key=var['aws_secret_access_key'])

    s3_client = s3_con.s3_client()

    keys = []
    kwargs = {'Bucket': var['s3_bucket'], 'Prefix': file_prefix}

    while True:
        resp = s3_client.list_objects_v2(**kwargs)
        for obj in resp['Contents']:
            
            if obj['Key'].endswith(file_type):
                keys.append(obj['Key'])

        try:
            kwargs['ContinuationToken'] = resp['NextContinuationToken']
        except KeyError:
            break

    return keys

def list_of_loaded_snowflake(con, table_name,load_date_str ):
    curs=con.cursor()
    return_list=[]
    sel_stmt = "select distinct FILE_NAME from LOAD_STATUS where TABLE_NAME='"+table_name+"' and LOAD_DATE='"+load_date_str+"'"
    pre_load_check = curs.execute(sel_stmt).fetchall()
    for items in pre_load_check:

        return_list.append(items[0])

    return set(return_list)

def s3_to_snow_load (file_name_lst, connection, gsp_vars, table_name, date_tag):
    DATE_LOAD_TAG = date_tag
    bucket_name = gsp_vars['s3_bucket']

    s3_con = BucketConn(key_id=gsp_vars['aws_access_key_id'],access_key=gsp_vars['aws_secret_access_key'])

    s3_client = s3_con.s3_client()
    bulk_data=[]
    
    for file_name in file_name_lst:
        response = s3_client.get_object(Bucket=bucket_name, Key=file_name)
        status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
        if status == 200:
            data = response['Body'].read()
            gzipfile = BytesIO(data)
            gzipfile = gzip.GzipFile(fileobj=gzipfile)
            try:
                # Content is a file with JSON object in each line
                content = gzipfile.read().decode('iso8859_16')
            except Exception as e:
                print("Unzip Error Occurecd.................")
                print("Error occured in:"+file_name)
                print("Error Details : "+str(e))

            content = content.split("\n") # List of JSON strings
            content = [i for i in content if i] # Removing Empty lines
            content = [json.loads(d) for d in content] #Convert JSOn strings to Json objects
            bulk_data.extend(content) #Append the JSON objects to bulk loader list
            #print("No: of rows in file : " + str(len(content)) +" No: of rows in Total : " +str(len(bulk_data)))

            del response
            del gzipfile
            del content
            gc.collect()

        else:
            print(f"Unsuccessful S3 get_object response. Status - {status}")

    print(" No: of rows in Total : " +str(len(bulk_data)))

    # Create pandas dataframe, note that every column is string.
    file_df = pd.DataFrame(bulk_data, dtype =str) 
    col_dict={}
    schema_sf_lst=[]
    schema_sf_lst_temp=[]
    for y in file_df.columns:
        if y not in col_dict.keys():
            refined_col = cameltosnake(y.replace(" ", "_").replace("&", "").replace("%", "").replace("(", "").replace(")", "").replace("+", "plus").replace("-", "_")).upper()
            col_dict[y]= refined_col

        else:
            print("Duplicate Columns :" + y)


    column_list =  col_dict.values()
    pd_schema = DataTypeMapper(column_list).pandas_datatype()
    sf_schema = DataTypeMapper(column_list).snow_datatype()
    sf_schema['DATE_LOAD_TAG'] = "VARCHAR(50)"
    
    for key,value in sf_schema.items():
        schema_sf_lst.append(key+" "+value)
        schema_sf_lst_temp.append(key+" VARCHAR")
    schema_sf_lst = ', '.join(schema_sf_lst)
    schema_sf_lst_temp = ', '.join(schema_sf_lst_temp)
    #print(schema_sf_lst)

    file_df = file_df.rename(columns = col_dict)
    file_df['DATE_LOAD_TAG'] = DATE_LOAD_TAG
    
    #Following statements can be printed out (usually in initial runs) to get DDLs

    sql_check_table = "SELECT count(*) FROM "+gsp_vars['snow_database']+".INFORMATION_SCHEMA.TABLES WHERE table_schema = '"+gsp_vars['snow_schema']+"' AND table_name = '"+table_name+"'"
    sql_create_table = "CREATE TABLE IF NOT EXISTS "+gsp_vars['snow_database']+"."+gsp_vars['snow_schema']+"."+table_name+" ("+schema_sf_lst+")"
    sql_create_table_temp = "CREATE TABLE IF NOT EXISTS "+gsp_vars['snow_database']+"."+gsp_vars['snow_schema']+"._TEMP_"+table_name+" ("+schema_sf_lst_temp+")"
    #print(sql_create_table)

    ''' Uncomment it during initial runs, once table is created in snow there is no need to re run it every time.
    cur = connection.cursor()
    cur.execute(sql_create_table_temp)
    cur.close()
    '''
    #print(file_df.dtypes)

    '''
    Loading logic:
                 Step 1 > Load pandas dataframe to _TEMP_GSPTABLE table. All columns are varchar.
                 Step 2 > Using a view _TEMP_GSPTABLE_DTYPE. Perform appropriate DATA TYPE comversions within snowflake. (Doing this using pandas can result in connector issues)
                 Step 3 > Select from view above and insert to main table GSPTABLE.
                 Step 4 > Truncate the _TEMP_GSPTABLE table.
    '''
    
    try:
        success, nchunks, nrows, output = write_pandas(conn=connection, df=file_df, table_name="_TEMP_"+table_name)

    except Exception as err:
        logging.error(err)
        logging.info(file_name_lst)
        raise AirflowException("Failed to load to temp table.")
    else:
        
        if success:
            sql_load_to_main = "insert into "+gsp_vars['snow_database']+"."+gsp_vars['snow_schema']+"."+table_name+"(select * from "+gsp_vars['snow_database']+"."+gsp_vars['snow_schema']+"._TEMP_"+table_name+"_DTYPE)"
            
            
            try:
                cur = connection.cursor()
                cur.execute(sql_load_to_main)
                print(sql_load_to_main)
                cur.close()
            except Exception as err:
                logging.error(err)
                raise AirflowException("Failed to load to main table.")
            else:
                dict_data = {"LOAD_DATE":[],"SCHEMA_NAME":[],"TABLE_NAME":[],"FILE_NAME":[]}
                #print("Loading to main table completed....")
                for file_name in file_name_lst:
                    dict_data["LOAD_DATE"].append(DATE_LOAD_TAG)
                    dict_data["SCHEMA_NAME"].append("GSPAPP")
                    dict_data["TABLE_NAME"].append(table_name)
                    dict_data["FILE_NAME"].append(file_name)
                load_status_df = pd.DataFrame(dict_data, dtype =str) 
                success_2, nchunks_2, nrows_2, output_2 = write_pandas(conn=connection, df=load_status_df, table_name='LOAD_STATUS')
                if success_2:
                    logging.info("Rows loaded to LOAD_STATUS:" + str(nrows_2))
                    #print("Truncating temporary table....")
                    sql_truncate = "truncate table "+gsp_vars['snow_database']+"."+gsp_vars['snow_schema']+"._TEMP_"+table_name
                    try:
                        cur = connection.cursor()
                        #print(sql_truncate)
                        cur.execute(sql_truncate)
                        cur.close()
                    except Exception as err:
                        logging.error(err)
                        raise AirflowException("Failed to truncate temp table.")
                    else:
                        print("Truncating temporary table Completed.....")
        ##logging.info(output)
        logging.info("Status of "+table_name+" load:" + str(success))
        logging.info("Chunks of "+table_name+" load:" + str(nchunks))
        logging.info("Rows loaded to "+table_name+" load:" + str(nrows))
    
    del bulk_data
    del file_df
    gc.collect()

def parallel_copy_trigger(q, connection_q, gsp_vars_q, table_name_q, date_tag_q):
    while True:
        file_lst_q = q.get()
        s3_to_snow_load (file_name_lst=file_lst_q, connection=connection_q, gsp_vars=gsp_vars_q, table_name = table_name_q, date_tag=date_tag_q)
        q.task_done()



def gspapp_ingest_to_snowflake (**kwargs):
    gspapp_vars = read_from_aws_sm_fn(sm_secretId_name)

    con = snowflake.connector.connect(
                                        user      = gspapp_vars['snow_user'],
                                        password  = gspapp_vars['snow_password'],
                                        account   = gspapp_vars['snow_account'],
                                        warehouse = gspapp_vars['snow_warehouse'],
                                        database  = gspapp_vars['snow_database'],
                                        schema    = gspapp_vars['snow_schema'],
                                        session_parameters={
                                                           'QUERY_TAG': 'gspapp_Load',
                                                           }
                                        )
    folders_list = gspapp_vars['folder_list_prefix'].split(',')
    #pprint(gspapp_vars['folder_list_prefix'])
    logging.info(folders_list)
    # Window  - htat files will be loaded in days
    N_DAYS_AGO = 100
    #Iterate through folders in s3 bucket - this is provided as comma seperated string from secrets manager.
    for src_folder in folders_list:
        logging.info("Processing folder :" + src_folder)
        table_name = src_folder
        table_name=table_name.replace(" ", "_").replace("&", "").replace("%", "").replace(":", "").replace("(", "").replace(")", "").replace(".", "_").replace("-", "_").upper()
        logging.info("Processing Table :" + table_name)
        lst_of_days = pd.date_range(datetime.today()-timedelta(days=N_DAYS_AGO), periods=N_DAYS_AGO, freq='D').strftime('%Y/%m/%d')
        list_of_days=[]
        for day in lst_of_days:
            list_of_days.append(str(day))
        list_of_days.reverse() # Script will start filling the latest day and go backwards..

        for day in list_of_days:
            logging.info("Processing date :" + str(day))
            list_of_files_src = list_files_src_s3(gspapp_vars, file_prefix = src_folder+"/"+str(day)+"/", file_type=".gz")
            already_loaded_to_snow = list_of_loaded_snowflake(con=con, table_name=table_name, load_date_str = day)

            set_list_of_files_dst = already_loaded_to_snow
            set_list_of_files_src = set(list_of_files_src)

            incremental_file_list = list(set_list_of_files_src.difference(set_list_of_files_dst))
            
            
            #Load the files in Parallel. If you are using a temporary table in Snow and then do data type conversion using snow this has to remain 1.
            PARALLEL = 1 # Never ever change this from 1 if leveraging  a single stage/temp table in the loading processes.
            
            #Group the files  to be preocessed in batches of N. I airflow runs outof memory, make N larger so eatch batch has lower no: of files.
            batched_list = batch_files(incremental_file_list,50)
            
            # Parallel processing mechanism
            q = Queue.Queue()

            for i in range(PARALLEL):
                t = Thread(target=parallel_copy_trigger, args=(q, con, gspapp_vars, table_name, day))
                t.daemon = True
                t.start()


            for file_list_q in batched_list:
                q.put(file_list_q)
            
            q.join()
            # EOF Parallel processing mechanism
            
        print("The Job completed successfully...............")
        

slack_vars_0 = read_from_aws_sm_fn('airflow/connections/slack')
def on_failure_callback(context):
    channel_name = '#etl-status-slack-notification'
    ti = context['task_instance']
    status_message = f"task: {ti.task_id } failed in dag: { ti.dag_id } "
    print(status_message)
    client = slack.WebClient(token=slack_vars_0['slack_token'])
    client.chat_postMessage(channel=channel_name,text=status_message)        

with DAG(
        dag_id=os.path.basename(__file__).replace(".py", ""),
        default_args=default_args,
        dagrun_timeout=timedelta(hours=12),
        schedule_interval="0 14 * * *",
        max_active_runs=1,
        start_date=days_ago(1),
) as dag:
    write_all_to_snow = PythonOperator(
        task_id="gspapp_ingest_to_snow",
        python_callable=gspapp_ingest_to_snowflake,
        provide_context=True,
        on_failure_callback = on_failure_callback
    )

write_all_to_snow
